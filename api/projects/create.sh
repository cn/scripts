#!/bin/bash
set -u -e
API_URL=http://127.0.0.1:8080/v2-beta
PROJECT_NAME="${1:-Test}"

API_PATH=/projects

METHOD=POST

DATA=$(mktemp /tmp/in.json.XXXXXX)

# DATA
cat << EOF | python -c "import json,sys; json.dump(json.load(sys.stdin),sys.stdout,separators=(',',':'));" > ${DATA}
{
  "description": "Created by script.",
  "hostRemoveDelaySeconds": 0,
  "members": [ { "externalId": "1a1", "externalIdType": "rancher_id", "role": "owner" } ],
  "name": "${PROJECT_NAME}",
  "projectTemplateId": "1pt1"
}
EOF

curl -s \
  -X "${METHOD}" \
  -H "Authorization: Bearer ${API_TOKEN}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d @${DATA} \
  "${API_URL}${API_PATH}" |
python -m json.tool

rm -f ${DATA}
set +u +e
