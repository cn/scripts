#!/bin/bash
set -u -e
API_URL=http://127.0.0.1:8080/v1

API_PATH=/projects

METHOD=GET

DATA=$(mktemp /tmp/in.json.XXXXXX)

# DATA
cat << EOF | python -c "import json,sys; json.dump(json.load(sys.stdin),sys.stdout,separators=(',',':'));" > ${DATA}
{}
EOF

curl -s \
  -X "${METHOD}" \
  -H "Authorization: Bearer ${API_TOKEN}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d @${DATA} \
  "${API_URL}${API_PATH}" |
python -m json.tool

rm -f ${DATA}
set +u +e
