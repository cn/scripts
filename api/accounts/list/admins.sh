#!/bin/bash
set -u -e
API_URL=http://127.0.0.1:8080/${API_VERSION:-v1}

API_PATH="/accounts?kind=admin"

METHOD=GET

DATA=$(mktemp /tmp/in.json.XXXXXX)

# DATA
cat << EOF | python -c "import json,sys; json.dump(json.load(sys.stdin),sys.stdout,separators=(',',':'));" > ${DATA}
{}
EOF

curl -s \
  -X "${METHOD}" \
  -H "Authorization: Bearer ${API_TOKEN}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d @${DATA} \
  "${API_URL}${API_PATH}" |
python -m json.tool

rm -f ${DATA}
set +u +e
