#!/bin/bash
echo >&2 -n "username: " && read API_USER
echo >&2 -n "password: " && { stty -echo; read API_KEY; stty echo; echo >&2 -e "\n"; }
API_URL=http://127.0.0.1:8080/v1

API_PATH=/token

METHOD=POST

DATA=$(mktemp /tmp/in.json.XXXXXX)

# DATA
set -e
cat << EOF | python -c "import json,sys; json.dump(json.load(sys.stdin),sys.stdout,separators=(',',':'));" > ${DATA}
{
  "code":"${API_USER}:${API_KEY}"
}
EOF

API_TOKEN=$(curl -s \
  -X "${METHOD}" \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d @${DATA} \
  "${API_URL}${API_PATH}" |
python -c "\
import json,sys
r = json.load(sys.stdin)

assert(r['enabled'])
print(r['jwt'])
")
set +e

echo "export API_TOKEN=${API_TOKEN}"

rm -f ${DATA}
