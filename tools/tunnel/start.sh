#!/bin/bash
LOCAL_PORT=${1:-3000}
LOCAL_ADDRESS=127.0.0.1
REMOTE_PORT=8080
REMOTE_ADDRESS=127.0.0.1
REMOTE_SSH_SERVER=root.drd.ovh
set -e
screen -X -S rancher_tunnel quit &>/dev/null || true
screen -S rancher_tunnel -d -m ssh -NL ${LOCAL_ADDRESS}:${LOCAL_PORT}:${REMOTE_ADDRESS}:${REMOTE_PORT} ${REMOTE_SSH_SERVER}
echo >&2 -e "Rancher web interface available at http://${LOCAL_ADDRESS}:${LOCAL_PORT}\n"
echo "xdg-open http://${LOCAL_ADDRESS}:${LOCAL_PORT}"
